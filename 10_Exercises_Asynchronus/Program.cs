﻿//-----------------10 exercises to practice Async programming-----------------


//CHuong trinh su dung await, async lay data tu API va in ra man hinh console
//using Newtonsoft.Json;

//var client = new HttpClient();
//var resquest = await client.GetAsync("https://jsonplaceholder.typicode.com/todos");
//var response = await resquest.Content.ReadAsStringAsync();
//var list = JsonConvert.DeserializeObject<List<Todo>>(response);
//foreach (var item in list)
//{
//    Console.WriteLine($"userId: {item.userId}, id: {item.id}, title: {item.title}, completed: {item.completed}");
//}
//class Todo
//{
//    public int userId { get; set; }
//    public int id { get; set; }
//    public string title { get; set; }
//    public bool completed { get; set; }
//}



//------------program that uses async and await to download a file from a URL and save it to disk-------------------------------
//var client = new HttpClient();
//var response = await client.GetAsync("https://example.com/file.txt");
//response.EnsureSuccessStatusCode(); // Ensure the response was successful.
//var fileName = new FileStream("file.txt", FileMode.Create);
//await response.Content.CopyToAsync(fileName);



//--------------program that uses async and await to read data from a file and display it on the console-----------------------

//string filePath = "D:\\output.txt";
//FileStream fileStream = new FileStream(filePath, FileMode.Open);
//StreamReader streamReader= new StreamReader(fileStream);
//string line;
//while ((line = await streamReader.ReadLineAsync()) != null)
//{
//    Console.WriteLine(line);
//}


//-------------program that uses async and await to calculate the factorial of a large number---------------------

//Console.Write("Input number: ");
//var n = int.Parse(Console.ReadLine());
//var result = await Task.Run(() => FactorialAsync(n));
//Console.WriteLine(result);


//async Task<int> FactorialAsync(int num)
//{
//    int result = 1;
//    for (int i = num; i > 0; i--)
//    {
//        result *= i;
//        await Task.Yield(); //Make us async right away
//    }
//    return result;
//}



//-----------program that uses async and await to simulate a long-running operation----------------------

//var countDown = async (int time) =>
//{
//	for (int i = time; i > 0; i--)
//	{
//		await Task.Delay(1000);
//		Console.WriteLine($"Loading... {i}");
//	}
//};
//await countDown(10);



//------------------program that uses async and await to perform a batch processing operation on a large dataset--------------

//class BatchProcessor
//{
//    public async Task ProcessBatch(IEnumerable<Data> data)
//    {
//        await Task.Run(() => {
//            Parallel.ForEach(data, async (item) => {
//                // Perform async operation on item
//                await DoAsyncOperation(item);
//            });
//        });
//    }

//    private async Task DoAsyncOperation(Data item)
//    {
//        // Perform async operation on item
//        await Task.Delay(1000);
//    }
//}
//class Data
//{
//    public int Id { get; set; }
//    public string Name { get; set; }
//    public int Value { get; set; }
//}


//-------------------program that uses async and await to generate a large amount of data and write it to a file--------------------

//----------------Use Async--------------------

//using System.Text;
//async Task<string> GenegrateDataAsync()
//{
//    StringBuilder stringBuilder = new StringBuilder();
//    await Task.Run(() =>
//    {
//        for (int i = 0; i < 10000; i++)
//        {
//            stringBuilder.AppendLine($"Data: {i}");
//        }
//    });
//    return stringBuilder.ToString();
//}

//async Task WriteToFileAsync(string filename)
//{
//    StreamWriter streamWriter = new StreamWriter("data.txt");
//    await streamWriter.WriteAsync(filename);
//}

//var data = await GenegrateDataAsync();
//await WriteToFileAsync(data);


//--------------------------Don't use Async-----------------------------

//string GenegrateData()
//{
//    StringBuilder stringBuilder = new StringBuilder();
//        for (int i = 0; i < 10000; i++)
//        {
//            stringBuilder.AppendLine($"Data: {i}");
//        }
//    return stringBuilder.ToString();
//}

//void WriteToFile(string filename)
//{
//    StreamWriter streamWriter = new StreamWriter("data1.txt");
//    streamWriter.WriteAsync(filename);
//}

//var data = GenegrateData();
//WriteToFile(data);


